/* init */
var ovr=false;                              // game over status
var grid=new Array();                       // the grid
var rows=20;                                // rows (min 20)
var cols=50;                                // cols (min 50)
var nln='\n';                               // line break
var markerHead='X';                         // board marker
var markerBody='+';                         // board marker
var markerFood='•';                         // board marker
var markerBlock='O';                        // board marker
var markerEmpty=' ';                        // board marker
var snakeMin=3;                             // snake min length
var snakeMax=18;                            // snake max length
var snakeBody=new Array();                  // snake body (r,c)
var snakeDir=0                              // snake direction (0:up,1:right,2:down,3:left)
var snakeDirChange=false;                   // snake direction change
var snakeSpeedMax=500;                      // snake speed default
var snakeSpeedMin=150;                      // snake speed minimum
var snakeSpeedUp=15;                        // snake speed up (on direction change)
var snakeSpeedDown=10;                      // snake speed down (on food catch)
var snakeSpeed=snakeSpeedMax;               // snake speed in ms
var gridItems=new Array('blocks','food');   // items on grid
var amtBlockUp=0.25;                        // block percentage up every level
var amtBlockMin=1;                          // block percentage minimum
var amtBlock=amtBlockMin;                   // block percentage
var amtFoodUp=0.15;                         // food percentage up every level
var amtFoodMin=2;                           // food percentage minimum
var amtFood=amtFoodMin;                     // food percentage
var grun=false;                             // game is running
var blnkcnt=0;                              // blink counter
var blnkamt=3;                              // blink amount
var refresher=false;                        // for interval refresher
var lvl=1;                                  // game level
var brds=true;                              // true:do not hit borders, false:pacman style



/* add class to element */
function addClassToElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  if(cls.includes(elmcls)==false){
    var newcls=cls.concat(' '+elmcls);
    document.getElementById(elmid).className=newcls;
  }
}

/* remove class from element */
function removeClassFromElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  var newcls=cls.split(elmcls).join('');
  document.getElementById(elmid).className=newcls;
}

/* generate box id for click */
function getBoxIdForRowAndCol(r,c){
  return 'box-'+r+'-'+c;
}

/* get the string with class names for element */
function getClassStringForElementById(elmid){
  return document.getElementById(elmid).className;
}

/* do click on element by id */
function clickOnElementById(elmid){
  document.getElementById(elmid).click();
}

/* hide cover layer */
function hideCover(){
  addClassToElementById('cover1','cover1hide');
  addClassToElementById('cover2','cover2hide');
}

/* center game */
function centerWrapper(){
  var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
  document.getElementById('wrapper').style.marginLeft=left+'px';
  var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
  document.getElementById('wrapper').style.marginTop=top+'px';
}



/* generate dynamic game grid */
function generateGrid(){
  console.log('generate grid');
  for(r=0;r<rows;r++){
    grid[r]=new Array();
    for(c=0;c<cols;c++){
      grid[r][c]=markerEmpty;
    }
  }
  console.log('generate grid items');
  // setup
  var cnt=0;
  var mrk='';
  // walk items
  gridItems.forEach(function(item,index){
    // item params
    switch(item){
      case 'blocks':
        cnt=Math.floor((rows*cols)*(amtBlock/100));
        mrk=markerBlock;
        break;
      case 'food':
      default:
        cnt=Math.floor((rows*cols)*(amtFood/100));
        mrk=markerFood;
        break;
    }
    // set items on empty spot
    while(cnt>=0){
      var rndrow=Math.floor(Math.random()*rows);
      var rndcol=Math.floor(Math.random()*cols);
      if(grid[rndrow][rndcol]==markerEmpty){
        grid[rndrow][rndcol]=mrk;
        cnt--;
      }
    }
  });
}

/* refresh grid */
function refreshGrid(){
  console.log('refresh grid');
  setGridHtml(generateGridHtml());
}

/* generate grid html */
function generateGridHtml(){
  console.log('generate grid html');
  // setup
  var html='';
  // dynamic grid
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      // draw (grid,food,block)
      var markerstr;
      switch(grid[r][c]){
        case markerFood:
          markerstr=markerFood;
          break;
        case markerBlock:
          markerstr=markerBlock;
          break;
        default:
          markerstr=markerEmpty;
      }
      if(grun==true){
        if(getTypeForPosInSnake(r,c)=='body'){markerstr=markerBody;}
        if(getTypeForPosInSnake(r,c)=='head'){markerstr=markerHead;}
      }
      html+='        <div class="box" attr-row="'+r+'" attr-col="'+c+'">'+markerstr+'</div>'+nln;
    }
    // clear row
    html+='        <div class="clear"></div>'+nln;
  }
  // return
  return html;
}

/* set grid html */
function setGridHtml(html){
  console.log('set grid html');
  document.getElementById('grid').innerHTML=html;
}



/* generate snake */
function generateSnake(){
  console.log('generate snake');
  // fresh start
  snakeBody=new Array();
  // set starting point (one length away from any blocks)
  var found=false;
  while(found==false){
    var rndrow=Math.floor(Math.random()*(rows-(2*snakeMin)))+snakeMin;
    var rndcol=Math.floor(Math.random()*(cols-(2*snakeMin)))+snakeMin;
    found=true;
    for(x=0;x<snakeMin;x++){
      if(grid[rndrow+x][rndcol]==markerBlock){found=false;}
      if(grid[rndrow-x][rndcol]==markerBlock){found=false;}
      if(grid[rndrow][rndcol+x]==markerBlock){found=false;}
      if(grid[rndrow][rndcol-x]==markerBlock){found=false;}
    }
  }
  snakeBody.push([rndrow,rndcol]);
  // set direction
  var sndir=Math.floor(Math.random()*4);
  snakeDir=sndir;
  // set speed depending on level
  snakeSpeed=snakeSpeedMax-((snakeSpeedUp*(lvl*2))-snakeSpeedUp);
  // create minimum snake body inverted for direction (0:up,1:right,2:down,3:left)
  for(x=0;x<snakeMin;x++){
    switch(snakeDir){
      case 0:
        rndrow++;
        break;
      case 1:
        rndcol--;
        break;
      case 2:
        rndrow--;
        break;
      case 3:
        rndcol++;
        break;
    }
    snakeBody.push([rndrow,rndcol]);
  }
}

/* get snake head positions */
function getSnakeHeadRow(){
  return snakeBody[0][0];
}
function getSnakeHeadCol(){
  return snakeBody[0][1];
}

/* change direction */
function snakeDirectionChange(direction=0){
  // if new is different to actual
  if(direction!=snakeDir){
    snakeDir=direction;
    snakeDirChange=true;
  }
}

/* snake speed change */
function snakeSpeedChangeUp(){
  snakeSpeed-=snakeSpeedUp;
}
function snakeSpeedChangeDown(){
  snakeSpeed+=snakeSpeedDown;
}
function keepSpeedInRange(){
  if(snakeSpeed<snakeSpeedMin){snakeSpeed=snakeSpeedMin;}
  if(snakeSpeed>snakeSpeedMax){snakeSpeed=snakeSpeedMax;}
}

/* make snake longer */
function addElementToSnakeBody(r,c){
  // add element as head
  snakeBody.unshift([r,c]);
  // if too long, remove last element
  if(snakeBody.length>snakeMax){snakeBody.pop();}
}

/* shift snake elements for movement */
function shiftSnakeBodyElements(r,c){
  // add element as head
  snakeBody.unshift([r,c]);
  // remove last element
  snakeBody.pop();
}

/* check if snake catched food */
function checkForFoodCatch(){
  // get head position
  var snrow=getSnakeHeadRow();
  var sncol=getSnakeHeadCol();
  // check
  try{
    if(grid[snrow][sncol]==markerFood){
      grid[snrow][sncol]=markerEmpty;
      snakeSpeedChangeDown();
    }
  }catch(err){
    // do nothing
  }
}

/* check if grid position is snake head or body or nothing */
function getTypeForPosInSnake(r,c){
  var shtype='';
  snakeBody.forEach(function(item,index) {
    if(index==0&&item[0]==r&&item[1]==c){shtype='head';}
    if(index>0&&item[0]==r&&item[1]==c){shtype='body';}
  });
  return shtype;
}

/* border hit can end game */
function snakeHitsBorder(snrow,sncol){
  var snhit=false;
  try{
    if(grid[snrow].length==0){snhit=false;}
    if(grid[snrow][sncol].length==0){snhit=false;}
  }catch(err){
    snhit=true;
  }
  return snhit;
}

/* what to do on a direction change */
function updateSnakeBody(snrow,sncol){
  if(snakeDirChange){
    addElementToSnakeBody(snrow,sncol);
    snakeSpeedChangeUp();
    snakeDirChange=false;
  }else{
    shiftSnakeBodyElements(snrow,sncol);
  }
}

/* move snake */
function moveSnake(){
  // check for end game
  checkForEndGame();
  // move snake
  if(ovr==false){
    // get head position
    var snrow=getSnakeHeadRow();
    var sncol=getSnakeHeadCol();
    // snake direction (0:up,1:right,2:down,3:left)
    switch(snakeDir){
      case 0:
        snrow--;
        break;
      case 1:
        sncol++;
        break;
      case 2:
        snrow++;
        break;
      case 3:
        sncol--;
        break;
    }
    // if borders are pacman style (no borders)
    if(brds==false){
      if(snrow<0){snrow=rows-1;}
      if(snrow>=rows){snrow=0;}
      if(sncol<0){sncol=cols-1;}
      if(sncol>=cols){sncol=0;}
    }
    // set new head position or make snake longer
    updateSnakeBody(snrow,sncol);
    // check for food catch
    checkForFoodCatch();
    // keep speed in range
    keepSpeedInRange();
    // timer for moving if game is runnung
    if(grun==true){setTimeout(()=>{moveSnake();},snakeSpeed);}
  }
}



/* is there still food to catch */
function getFoodAmount(){
  // setup
  var amt=0;
  // check for places with 'ship' (>0)
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      if(grid[r][c]==markerFood){amt++;}
    }
  }
  // return
  return amt;
}



/* did player win or not */
function checkForEndGame(){
  console.log('check for end game');
  // setup
  var gmend=false;
  // no possible moves for player
  if(getFoodAmount()==0){gmend=true;}
  // snake head position
  var snrow=getSnakeHeadRow();
  var sncol=getSnakeHeadCol();
  // snake hit border (if switched on)
  if(gmend==false&&brds==true&&snakeHitsBorder(snrow,sncol)==true){gmend=true;}
  // snake head hits body
  if(gmend==false&&getTypeForPosInSnake(snrow,sncol)=='body'){gmend=true;}
  // snake head hits block
  if(gmend==false&&grid[snrow][sncol]==markerBlock){gmend=true;}
  // end game if needed
  if(gmend==true){endGame();}
}

/* end of game */
function endGame(){
  console.log('game ended');
  // stop refresher
  clearInterval(refresher);
  // set flag
  ovr=true;
  grun=false;
  // set color
  var clsname='end';
  if(getFoodAmount()==0){clsname='win';}
  addClassToElementById('grid',clsname+'game');
}

/* start game */
function startGame(){
  console.log('start game');
  // refresh screen with interval
  refresher=setInterval(()=>{refreshGrid();},100);
  // and go
  generateSnake();
  grun=true;
  moveSnake();
}

/* intro blinking */
function introGame(){
  console.log('intro game');
  // only if game is not running
  if(grun==false){
    if(blnkcnt==blnkamt){
      startGame();
    }else{
      var clsstr=getClassStringForElementById('grid');
      if(clsstr.includes('blinkon')){
        removeClassFromElementById('grid','blinkon');
        blnkcnt++;
      }else{
        addClassToElementById('grid','blinkon');
      }
    }
    setTimeout(()=>{introGame();},500);
  }
}

/* set some params depending on game level */
function setGridParamsForLevel(){
  console.log('set params for level');
  // get game status (win or end)
  var clsstr=getClassStringForElementById('grid');
  // on game end start new
  if(clsstr.includes('endgame')){
    lvl=1;
    amtBlock=amtBlockMin;
    amtFood=amtFoodMin;
  }
  // on win level up
  if(clsstr.includes('wingame')){
    lvl++;
    amtBlock+=(amtBlockUp*lvl)-amtBlockUp;
    amtFood+=(amtFoodUp*lvl)-amtFoodUp;
  }
  // remove marker colors
  removeClassFromElementById('grid','endgame');
  removeClassFromElementById('grid','wingame');
  // display level
  document.getElementById('lvl').innerHTML=lvl;
}



/* init game */
function init(){
  console.log('init');
  // setting up everything
  setGridParamsForLevel();
  blnkcnt=0;
  grid=new Array();
  generateGrid();
  setGridHtml(generateGridHtml());
  ovr=false;
  grun=false;
  // and go
  console.log('ready!');
}



/* listener for key */
document.addEventListener('keydown',(event)=>{
  // 38:up, 40:down, 37:left, 39:right
  switch(event.which){
    case 38:
      snakeDirectionChange(0);
      break;
    case 39:
      snakeDirectionChange(1);
      break;
    case 40:
      snakeDirectionChange(2);
      break;
    case 37:
      snakeDirectionChange(3);
      break;
    // n(ew)
    case 78:
      if(ovr==true){clickOnElementById('new');}
      break;
    // s(tart)
    case 83:
      if(ovr==false&&grun==false){clickOnElementById('start');}
      break;
  }
},false);

/* listener for click */
document.addEventListener('click',(event)=>{
  if(event.target.matches('.button')){
    var action=event.target.getAttribute('attr-action');
    if(ovr==true&&action=='new'){init();}
    if(ovr==false&&grun==false&&action=='start'){introGame();}
  }
},false);



let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    init();
    centerWrapper();
    hideCover();
  }
},250);
